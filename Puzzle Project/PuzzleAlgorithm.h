#pragma once

#include "State.h"
#include "Node.h"

#include <memory>
#include <queue>
#include <unordered_set>

class NodeEqual {
public:
	bool operator()(std::shared_ptr<Node> first, std::shared_ptr<Node> second) const {
		return *first == *second;
	}
};

struct NodeHash {
public:
	size_t operator()(std::shared_ptr<Node> node) const {
		return node->getHashCode();
	}
};

class PuzzleAlgorithm
{
public:
	PuzzleAlgorithm(const std::shared_ptr<Node>& initialState, const std::shared_ptr<Node>& finalState);

public:
	std::shared_ptr<Node> breadthFirstSearch();

private: 
	void expandTree(std::shared_ptr<Node>& node);

private:
	std::shared_ptr<Node> m_state;
	std::shared_ptr<Node> m_finalState;
	std::queue<std::shared_ptr<Node>> m_fringe;
	std::unordered_set<std::shared_ptr<Node>, NodeHash, NodeEqual> m_closed;
	std::unordered_set<std::shared_ptr<Node>, NodeHash, NodeEqual> m_fringeSet;
};