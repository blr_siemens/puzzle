#include "Node.h"
#include "Directions.h"


Node::Node(const State& value) : m_value (value)
{
	/*EMPTY*/
}

std::vector<std::shared_ptr<Node>> Node::getSuccesors() 
{
	auto blankPostion = m_value.findBlank();
	for (auto& pair : directions::validDirections)
	{
		///Pe dos practic
		auto newPos = std::make_pair(blankPostion.first + pair.second.first, blankPostion.second + pair.second.second);
		if (m_value.isNotOutOfBounds(newPos))
		{
			auto newState = std::make_shared<Node>(Node(m_value.swap(pair.second)));
			m_succesors.emplace_back(newState);
		}
	}

	return m_succesors;
}

std::shared_ptr<Node> Node::getParent() const
{
	return m_parent;
}

bool Node::operator==(const Node & other) const
{
	return m_value == other.m_value;
}

bool Node::operator!=(const Node & other) const
{
	return m_value != other.m_value;
}

void Node::print() const
{
	m_value.print();
}

void Node::setParent(std::shared_ptr<Node>& node)
{
	m_parent = node;
}

size_t Node::getHashCode() const
{
	return m_value.getHashCode();
}
