#pragma once

#include <vector>
#include <iostream>

class State
{
public:
	enum class Direction
	{
		Up,
		Down,
		Left,
		Right
	};
public:
	State(const std::vector<std::vector<int>>& container = std::vector<std::vector<int>>());
	State(const State& other);
	State(State&& other);

	State& operator=(State&& other);
	State& operator=(const State& other);
	
	bool operator ==(const State& other) const;
	bool operator !=(const State& other) const;

	std::pair<int, int> findBlank() const;
	State swap(std::pair<int, int>& newPos);
	bool isNotOutOfBounds(std::pair<int, int>& pos);
	bool isValid();
	void print() const;
	size_t getHashCode() const;

private:

	bool isEqual(const State& other) const;
	int numberOfInversions();

private:
	std::vector<std::vector<int>> m_container;
//public:
//	static std::map<Direction, std::pair<int, int>> m_directions;
};

