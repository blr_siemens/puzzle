#include "Game.h"
#include "PuzzleAlgorithm.h"
#include "Node.h"
#include <iostream>


Game::Game(const std::string& pathName1, const std::string& pathName2)
{
	LoadStates(pathName1 , pathName2);

	if (m_initialState.isValid())
	{
		std::cout << "Yes \n";
		PuzzleAlgorithm puzz(std::make_shared<Node>(Node(m_initialState)) , std::make_shared<Node>(Node(m_finalState)));
		auto lastNode = puzz.breadthFirstSearch();
	}

}


std::vector<std::vector<int>> Game::LoadState(const std::string& pathname)
{
	std::vector<std::vector<int>> v;
	std::ifstream ifs(pathname);
	std::string tempstr;
	int tempint;

	if (ifs.is_open())
	{
		while (std::getline(ifs, tempstr))
		{
			std::istringstream iss(tempstr);
			std::vector<int> tempv;
			while (iss >> tempint)
			{
				tempv.push_back(tempint);
			}
			v.push_back(tempv);
		}
	}
	else
	{
		//exc
	}

	ifs.close();

	return v;
}

void Game::LoadStates(const std::string& pathName1 , const std::string& pathName2)
{
	m_initialState = LoadState(pathName1);
	m_finalState = LoadState(pathName2);
}
