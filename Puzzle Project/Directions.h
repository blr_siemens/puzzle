#pragma once
#include "State.h"

namespace directions
{
	static std::vector<std::pair<State::Direction, std::pair<int, int>>> validDirections
	{
		std::make_pair < State::Direction , std::pair<int,int>>(State::Direction::Right , std::make_pair<int,int>(0,+1)),
		std::make_pair < State::Direction , std::pair<int,int>>(State::Direction::Up , std::make_pair<int,int>(-1,0)),
		std::make_pair < State::Direction , std::pair<int,int>>(State::Direction::Down , std::make_pair<int,int>(+1,0)),
		std::make_pair < State::Direction , std::pair<int,int>>(State::Direction::Left , std::make_pair<int,int>(0,-1))
	};

}