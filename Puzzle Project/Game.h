#pragma once

#include <string>
#include <fstream>
#include <sstream>

#include "State.h"


class Game
{
public:
	Game(const std::string& pathName1, const std::string& pathName2);

	~Game() = default;

private:
	void LoadStates(const std::string& pathName1, const std::string& pathName2);
	std::vector<std::vector<int>> LoadState(const std::string& pathname);

private:
	State m_initialState;
	State m_finalState;

};

