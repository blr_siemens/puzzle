#pragma once

#include "State.h"

#include <vector>
#include <memory>

class Node
{
public:
	Node(const State& value);

	~Node() = default;

public:
	std::vector<std::shared_ptr<Node>> getSuccesors();
	std::shared_ptr<Node> getParent() const;

	bool operator ==(const Node& other) const;
	bool operator !=(const Node& other) const;

	void print() const;
	void setParent(std::shared_ptr<Node>& node);
	size_t getHashCode() const;
private:
	State m_value;
	std::shared_ptr<Node> m_parent;
	std::vector<std::shared_ptr<Node>> m_succesors;
	size_t m_level;
};