#include "State.h"



State::State(const std::vector<std::vector<int>>& container)
	: m_container(container)
{
}

State::State(const State & other)
{
	*this = other;
}

State::State(State && other)
{
	*this = std::move(other);
}

State & State::operator=(State && other)
{
	m_container = other.m_container;
	other.m_container.clear();
	return *this;
}

State & State::operator=(const State & other)
{
	m_container = other.m_container;
	return *this;
}

bool State::operator==(const State & other) const
{
	return isEqual(other);
}

bool State::operator!=(const State & other) const
{
	return !isEqual(other);
}

std::pair<int, int> State::findBlank() const
{
	for (int i = 0; i < m_container.size(); i++)
	{
		for (int j = 0; j < m_container.size(); j++)
		{
			if (m_container[i][j] == 0)
				return std::make_pair(i, j);
		}
	}
	return std::make_pair(0, 0);
}

State State::swap(std::pair<int, int>& newPos)
{
	auto pos = findBlank();
	auto newState = State(*this);
	std::swap(newState.m_container[pos.first][pos.second], newState.m_container[pos.first + newPos.first][pos.second + newPos.second]);
	return newState;
}

bool State::isEqual(const State & other) const
{
	for (size_t i = 0; i < m_container.size(); i++)
	{
		for (size_t j = 0; j < m_container.size(); j++)
		{
			if (m_container[i][j] != other.m_container[i][j])
				return false;
		}
	}
	return true;
}

bool State::isValid()
{
	/*( (grid width odd) && (#inversions even) )  ||  ( (grid width even) && ((blank on odd row from bottom) == (#inversions even)) )*/
	int inversionsNumber = numberOfInversions();

	if (!(m_container.size() % 2 == 0) &&
		(inversionsNumber) % 2 != 0)
		return false;

	bool blankOnOdd = findBlank().first % 2 == 0;
	
	if (!(m_container.size() % 2 != 0) &&
		blankOnOdd && inversionsNumber % 2 != 0)
		return false;

	return true;


}

void State::print() const
{
	std::cout << "\n";
	for (size_t i = 0; i < m_container.size(); i++)
	{
		for (size_t j = 0; j < m_container.size(); j++)
		{
			std::cout << m_container[i][j] << " ";
		}
		std::cout << "\n";
	}
}

size_t State::getHashCode() const
{
	size_t sum = 0;
	for (size_t i = 0; i < m_container.size(); i++)
	{
		for (size_t j = 0; j < m_container.size(); j++)
		{
			sum += i * j + m_container[i][j];
		}
	}
	return sum;
}

int State::numberOfInversions()
{
	int inversionsNumber = 0;

	for (int i = 0; i < m_container.size(); ++i)
		for (int j = i + 1; j < m_container.size() - 1; ++j)
		{
			int currentNumber = m_container.at(i).at(j);
			int nextNumber = m_container.at(i).at(j + 1);

			if (currentNumber > nextNumber)
				for (int icpy = i; icpy < m_container.size(); ++icpy)
					for (int jcpy = j; jcpy < m_container.size(); ++jcpy)
					{
						int compareNumber = m_container.at(icpy).at(jcpy);
						if (compareNumber < currentNumber)
						{
							++inversionsNumber;
						}
					}
		}
	return inversionsNumber;
}

bool State::isNotOutOfBounds(std::pair<int, int>& pos)
{
	if (pos.first < m_container.size() && pos.first >= 0 && pos.second < m_container.size() && pos.second >= 0)
		return true;
	return false;
}
