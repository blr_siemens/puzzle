#include "PuzzleAlgorithm.h"

#include <iostream>

PuzzleAlgorithm::PuzzleAlgorithm(const std::shared_ptr<Node>& initialState, const std::shared_ptr<Node>& finalState)
	: m_state(initialState), 
	m_finalState(finalState)
{
}

std::shared_ptr<Node> PuzzleAlgorithm::breadthFirstSearch()
{
	bool foundSolution = false;
	m_fringeSet.emplace(m_state);
	m_fringe.push(m_state);
	while (!foundSolution)
	{
		std::cout << "Fringe size " << m_fringe.size() << std::endl;
		std::cout << "Closed size " << m_closed.size() << std::endl;
		m_state = m_fringe.front();
		if (*m_state == *m_finalState)
		{
			foundSolution = true;
			break;
		}
		expandTree(m_state);
		m_closed.emplace(m_fringe.front());
		m_fringe.pop();
		/*for (auto& elem : m_closed)
		{
			elem->print();
		}*/
	}
	return m_state;
}

void PuzzleAlgorithm::expandTree(std::shared_ptr<Node>& node)
{
	auto nodes = node->getSuccesors();
	for (auto& elem : nodes)
	{
		elem->setParent(node);
		auto a = m_fringeSet.insert(elem);
		if (a.second)
			m_fringe.push(elem);
	}
}
